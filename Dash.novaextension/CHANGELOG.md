## Version 1.2.1

* fixed bug that prevented scss files from working correctly

## Version 1.2

* added Elixir (Matt Stubbs)

## Version 1.1

* added Advanced PHP

## Version 1.0

Initial release

